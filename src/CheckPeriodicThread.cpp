static const char *RcsId = "$Header: /home/cvsadm/cvsroot/fermi/servers/hdb++/hdb++es/src/CheckPeriodicThread.cpp,v 1.6 2014-03-06 15:21:43 graziano Exp $";
//+=============================================================================
//
// file :         CheckPeriodicThread.cpp
//
// description :  C++ source for thread management
// project :      TANGO Device Server
//
// $Author: graziano $
//
// $Revision: 1.6 $
//
// $Log: CheckPeriodicThread.cpp,v $
//
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================


#include "CheckPeriodicThread.h"
#include "HdbDevice.h"
#include "SubscribeThread.h"

namespace HdbEventSubscriber_ns
{

const std::chrono::milliseconds default_period(1000);

//=============================================================================
//=============================================================================
CheckPeriodicThread::CheckPeriodicThread(HdbDevice *dev, std::chrono::milliseconds tol): AbortableThread(dev->_device, std::chrono::duration<double>::zero())
                                                          , delay_tolerance(tol)
{
	hdb_dev = dev;
}

//=============================================================================
//=============================================================================
auto CheckPeriodicThread::init_abort_loop() -> void
{
    INFO_STREAM << "CheckPeriodicThread delay_tolerance_ms="<<delay_tolerance.count()<<" id="<<omni_thread::self()->id()<<std::endl;
}

//=============================================================================
//=============================================================================
auto CheckPeriodicThread::get_abort_loop_period() -> std::chrono::milliseconds
{
    auto min_time_to_timeout = std::chrono::milliseconds::zero();
    try
    {
        min_time_to_timeout = hdb_dev->shared->check_periodic_event_timeout(delay_tolerance);
    }catch(Tango::DevFailed &e)
    {
    }

    //sleep 1 second more than the first expiration time calculated
    return default_period + min_time_to_timeout;
}

//=============================================================================
//=============================================================================
auto CheckPeriodicThread::finalize_abort_loop() -> void
{
    INFO_STREAM <<"CheckPeriodicThread::"<< __func__<<": exiting..."<<std::endl;
}

//=============================================================================
//=============================================================================
auto CheckPeriodicThread::run_thread_loop() -> void
{
}

}	//	namespace
