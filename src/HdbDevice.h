//=============================================================================
//
// file :        HdbEventHandler.h
//
// description : Include for the HDbDevice class.
//
// project :	Tango Device Server
//
// $Author: graziano $
//
// $Revision: 1.5 $
//
// $Log: HdbDevice.h,v $
// Revision 1.5  2014-03-06 15:21:43  graziano
// StartArchivingAtStartup,
// start_all and stop_all,
// archiving of first event received at subscribe
//
// Revision 1.4  2013-09-02 12:19:11  graziano
// cleaned
//
// Revision 1.3  2013-08-26 13:25:44  graziano
// added fix_tango_host
//
// Revision 1.2  2013-08-14 13:10:07  graziano
// development
//
// Revision 1.1  2013-07-17 13:37:43  graziano
// *** empty log message ***
//
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================

#ifndef _HDBDEVICE_H
#define _HDBDEVICE_H

//#define MAX_ATTRIBUTES		10000
#define CONTEXT_KEY		"strategy"
#define TTL_KEY			"ttl"
#define ALWAYS_CONTEXT	"ALWAYS"
#define ALWAYS_CONTEXT_DESC	"Always stored"

#include <tango/tango.h>
#include "Consts.h"
#include <thread> // fix std::this_thread
#include <chrono>

/**
 * @author	$Author: graziano $
 * @version	$Revision: 1.5 $
 */

 //	constants definitions here.
 //-----------------------------------------------
#define STATUS_SUBSCRIBED	std::string("Subscribed")
#define STATUS_DB_ERROR		std::string("Storing Error")


namespace HdbEventSubscriber_ns
{

/*
 * Output operator overload for Tango::DevFailed
 */
static inline std::ostream &operator<<(std::ostream &ss, const Tango::DevFailed &devFailed)
{
    for (CORBA::ULong i = 0; i < devFailed.errors.length(); ++i)
    {
        const auto& error = devFailed.errors[i];
        ss << "\tOrigin: " << error.origin << std::endl;
        ss << "\tSeverity: " << error.severity << std::endl;
        ss << "\tReason: " << error.reason << std::endl;
        ss << "\tDescription: " << error.desc << std::endl;
    }
    return ss;
}

class PollerThread;
class StatsThread;
class CheckPeriodicThread;
class PushThread;
class SubscribeThread;
class SharedData;

//==========================================================
/**
 * Class Description:
 * This class manage a HdbAccess class to store value in TACO history database
 */
//==========================================================
class HdbDevice : public Tango::CallBack, public Tango::LogAdapter
{
    private:
        std::string current_context;
        std::chrono::time_point<std::chrono::system_clock> last_stat;
        std::chrono::duration<double>		period;
        std::chrono::duration<double>		poller_period;
        std::chrono::duration<double>		stats_window;
        std::chrono::milliseconds		check_periodic_delay;
public:
	//	Data members here
	//-----------------------------------------
	std::unique_ptr<SubscribeThread, std::function<void(SubscribeThread*)>> thread;
	std::unique_ptr<PushThread, std::function<void(PushThread*)>> push_thread;
	std::unique_ptr<StatsThread, std::function<void(StatsThread*)>> stats_thread;
	std::unique_ptr<CheckPeriodicThread, std::function<void(CheckPeriodicThread*)>> check_periodic_thread;
	std::unique_ptr<PollerThread, std::function<void(PollerThread*)>> poller_thread;
	bool				subscribe_change;
	bool				list_from_file;
        std::string				list_file_error;
        std::string				list_filename;
	/**
	 *	Shared data
	 */
	std::shared_ptr<SharedData> shared;
	Tango::DeviceImpl *_device;
        std::map<std::string, std::string> domain_map;

	Tango::DevDouble	AttributeRecordFreq;
	Tango::DevDouble	AttributeFailureFreq;
	Tango::DevDouble	AttributeRecordFreqList[MAX_ATTRIBUTES];
	Tango::DevDouble	AttributeFailureFreqList[MAX_ATTRIBUTES];
	Tango::DevLong		AttributeEventNumberList[MAX_ATTRIBUTES];
	Tango::DevLong		AttributePendingNumber;
	Tango::DevLong		AttributeMaxPendingNumber;

	Tango::DevLong	attr_AttributeOkNumber_read;
	Tango::DevLong	attr_AttributeNokNumber_read;
	Tango::DevLong	attr_AttributeNumber_read;
	Tango::DevLong	attr_AttributeStartedNumber_read;
	Tango::DevLong	attr_AttributePausedNumber_read;
	Tango::DevLong	attr_AttributeStoppedNumber_read;

	Tango::DevDouble	attr_AttributeMaxStoreTime_read;
	Tango::DevDouble	attr_AttributeMinStoreTime_read;
	Tango::DevDouble	attr_AttributeMaxProcessingTime_read;
	Tango::DevDouble	attr_AttributeMinProcessingTime_read;

	Tango::DevString	attr_AttributeList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributeOkList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributeNokList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributePendingList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributeStartedList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributePausedList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributeStoppedList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributeErrorList_read[MAX_ATTRIBUTES];
	Tango::DevString	attr_AttributeContextList_read[MAX_ATTRIBUTES];

	Tango::DevULong		attr_AttributeTTLList_read[MAX_ATTRIBUTES];

        std::vector<std::string> attribute_list_str;
        std::size_t attribute_list_str_size;
	std::vector<std::string> attribute_ok_list_str;
	std::size_t attribute_ok_list_str_size;
	std::vector<std::string> attribute_nok_list_str;
	std::size_t attribute_nok_list_str_size;
	std::vector<std::string> attribute_pending_list_str;
	std::size_t attribute_pending_list_str_size;
	std::vector<std::string> attribute_started_list_str;
	std::size_t attribute_started_list_str_size;
	std::vector<std::string> attribute_paused_list_str;
	std::size_t attribute_paused_list_str_size;
	std::vector<std::string> attribute_stopped_list_str;
	std::size_t attribute_stopped_list_str_size;
	std::vector<std::string> attribute_error_list_str;
	std::size_t attribute_error_list_str_size;
	std::vector<std::string> attribute_context_list_str;
	std::size_t attribute_context_list_str_size;

        std::map<std::string,std::string> contexts_map;
        std::map<std::string,std::string> contexts_map_upper;
        std::string defaultStrategy;

	/**
	 * Constructs a newly allocated Command object.
	 *
	 *	@param devname 	Device Name
	 *	@param p	 	Period to retry subscribe event
	 *	@param pp	 	Poller thread Period
	 *	@param s	 	Period to compute statistics
	 *	@param c	 	Delay before timeout on periodic events
	 *	@param ch	 	Subscribe to change event if archive event is not used
	 */
	HdbDevice(int p, int pp, int s, int c, bool ch, const std::string &fn, Tango::DeviceImpl *device);
	~HdbDevice();
	/**
	 * initialize object
	 */
	void initialize();

	/**
	 * Add a new signal.
	 */
	void add(const std::string &signame, std::vector<std::string>& contexts, int data_type, int data_format, int write_type);
	/**
	 * AddRemove a signal in the list.
	 */
	void remove(const std::string &signame);
	/**
	 * Update contexts for a signal.
	 */
	void update(const std::string &signame, std::vector<std::string>& contexts);
	/**
	 * Update ttl for a signal.
	 */
	void updatettl(const std::string &signame, long ttl);
	/**
	 *	Update SignalList property
	 */
	void put_signal_property(std::vector<std::string> &prop);

	/**
	 *	Return the list of signals
	 */
	void get_sig_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals on error
	 */
	void get_sig_on_error_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals not on error
	 */
	void get_sig_not_on_error_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals started
	 */
	void get_sig_started_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals not_started
	 */
	void get_sig_not_started_list(std::vector<std::string> &);
	/**
	 *	Return the list errors
	 */
	bool get_error_list(std::vector<std::string> &);
	/**
	 *	Populate the list of event received numbers
	 */
	void  get_event_number_list();
	/**
	 *	Return the number of signals on error
	 */
	int  get_sig_on_error_num();
	/**
	 *	Return the number of signals not on error
	 */
	int  get_sig_not_on_error_num();
	/**
	 *	Return the number of signals started
	 */
	int  get_sig_started_num();
	/**
	 *	Return the number of signals not started
	 */
	int  get_sig_not_started_num();
	/**
	 *	Return the status of specified signal
	 */
	auto get_sig_status(const std::string &signame) -> std::string;
	/**
	 *	Return ALARM if at list one signal is not subscribed.
	 */
	virtual Tango::DevState subcribing_state();
	/**
	 *	Manage attribute received an error event
	 */
	void error_attribute(Tango::EventData *data);
	/**
	 *	Calculate statistics of the HDB storage time/attribute
	 */
	void storage_time(Tango::EventData *data, double elapsed);
	/**
	 *	Returns how many signals are waiting to be stored
	 */
	 auto get_max_waiting() const -> int;
	/**
	 *	Returns how many signals are waiting to be stored
	 */
	 auto nb_cmd_waiting() const -> int;
	/**
	 *	Returns the list of signals waiting to be stored
	 */
	void get_sig_list_waiting(std::vector<std::string> &) const;
	/**
	 *	Reset statistic counters
	 */
	 void reset_statistics();
	/**
	 *	Reset statistic freq counters
	 */
	 void reset_freq_statistics();
	/**
	 *	Return the complete, started  and stopped lists of signals
	 */
	bool  get_lists(std::vector<std::string> &_list, std::vector<std::string> &_start_list, std::vector<std::string> &_pause_list, std::vector<std::string> &_stop_list, std::vector<std::string> &_context_list, Tango::DevULong *ttl_list);
	/**
	 *	Check if fqdn, otherwise fix it
	 */
	void fix_tango_host(const std::string &attr, std::string& fixed);
	/**
	 *	Check if full domain name, otherwise fix it
	 */
	void add_domain(const std::string &attr, std::string& with_domain);
	/**
	 *	Set current context and start the attributes that should start.
	 */
	void set_context_and_start_attributes(const std::string& context);
	/**
	 *	Start an attribute.
	 */
	void start_attribute(const std::string& attribute);
	/**
	 *	Stop an attribute.
	 */
	void stop_attribute(const std::string& attribute);
	/**
	 *	Pause an attribute.
	 */
	void pause_attribute(const std::string& attribute);

        auto get_last_stat() -> time_t
        {
            return std::chrono::system_clock::to_time_t(last_stat);
        };
	/**
	 *	Compare without domain
	 */
	static auto compare_without_domain(const std::string &str1, const std::string &str2) -> bool;
	/**
	 *	explode a string in multiple strings using separator
	 */
	static void string_explode(const std::string &str, const std::string &separator, std::vector<std::string>& results);

        template<typename T>
        void push_events(const std::string& att_name, T* data, bool sleep = false);
        template<typename T>
        void push_events(const std::string& att_name, T* data, long size, bool sleep = false);

protected :	
	/**
	 * Read signal list in database as property.
	 *
	 */
	void get_hdb_signal_list(std::vector<std::string> &);
	/**
	 * Build signal vector
	 *
	 *	@param list 	signal names vector
	 */
	void build_signal_vector(const std::vector<std::string> &, const std::string &);
	/**
	 *	Store double (vector) in HDB
	 */
	void store_double(std::string &name, std::vector<double> values, int time);
	/**
	 *	Store long (vector) in HDB
	 */
	void store_long(std::string &name, std::vector<long int> values, int time);
	/**
	 *	Store string (vector) in HDB
	 */
	void store_string(std::string &name, std::vector<std::string> values, int time);

	virtual void push_event(Tango::EventData *data);
	virtual void push_event(Tango::AttrConfEventData* data);
private:	
	/**
	 *	Returns the tango host and signal name (tango host has been added since tango 7.1.1)
	 */
	static void get_tango_host_and_signal_name(const std::string &signame, std::string& tango_host, std::string& name);
	/**
	 *	Remove domain
	 */
	static auto remove_domain(const std::string &str) -> std::string;
};




template<typename T>
void HdbDevice::push_events(const std::string& attr_name, T* data, bool sleep)
{
    try
    {
        _device->push_change_event(attr_name, data);
        _device->push_archive_event(attr_name, data);
    }
    catch(Tango::DevFailed &e){}

    // TODO is this needed ?
    if(sleep)
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
}

template<typename T>
void HdbDevice::push_events(const std::string& attr_name, T* data, long size, bool sleep)
{
    try
    {
        _device->push_change_event(attr_name, data, size);
        _device->push_archive_event(attr_name, data, size);
    }
    catch(Tango::DevFailed &e){}

    // TODO is this needed ?
    if(sleep)
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
}
}	// namespace_ns

#endif	// _HDBDEVICE_H
