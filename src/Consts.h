//=============================================================================
//
// file :        Consts.h
//
// description : Defines all kind of constants
//
// project :	Tango Device Server
//
// $Author: graziano $
//
// $Revision: 1.5 $
//
// $Log: CheckPeriodicThread.h,v $
//
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================

#ifndef _CONSTS_H
#define _CONSTS_H

/**
 * @author	$Author: graziano $
 * @version	$Revision: 1.5 $
 */

 //	constants definitions here.
 //-----------------------------------------------

namespace HdbEventSubscriber_ns
{
    const unsigned int tango_prefix_length = 8;
    const unsigned int MAX_ATTRIBUTES = 10000;
    constexpr int DEFAULT_TTL = 0;
}	// namespace_ns

#endif	// _CONSTS_H
