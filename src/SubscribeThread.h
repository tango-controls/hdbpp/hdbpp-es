//=============================================================================
//
// file :        HdbEventHandler.h
//
// description : Include for the HDbDevice class.
//
// project :	Tango Device Server
//
// $Author: graziano $
//
// $Revision: 1.5 $
//
// $Log: SubscribeThread.h,v $
// Revision 1.5  2014-03-06 15:21:43  graziano
// StartArchivingAtStartup,
// start_all and stop_all,
// archiving of first event received at subscribe
//
// Revision 1.4  2013-09-24 08:42:21  graziano
// bug fixing
//
// Revision 1.3  2013-09-02 12:11:32  graziano
// cleaned
//
// Revision 1.2  2013-08-23 10:04:53  graziano
// development
//
// Revision 1.1  2013-07-17 13:37:43  graziano
// *** empty log message ***
//
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================

#ifndef _SUBSCRIBE_THREAD_H
#define _SUBSCRIBE_THREAD_H

#include <tango/tango.h>
#include <stdint.h>
#include <string>
#include <chrono>
#include "Consts.h"
#include <mutex>


/**
 * @author	$Author: graziano $
 * @version	$Revision: 1.5 $
 */

 //	constants definitions here.
 //-----------------------------------------------
#define	ERR			-1
#define	NOTHING		0
#define	UPDATE_PROP	1

namespace HdbEventSubscriber_ns
{

typedef struct 
{
	std::string	name;
        std::string	devname;
        std::string	attname;
        std::string	status;
	int		data_type;
	Tango::AttrDataFormat		data_format;
	int		write_type;
	int max_dim_x;
	int max_dim_y;
        std::shared_ptr<Tango::AttributeProxy> attr;
	Tango::DevState			evstate;
	bool 	first;
	bool 	first_err;
	int		event_id;
	int		event_conf_id;
	uint32_t okev_counter;
	uint32_t okev_counter_freq;
	uint32_t nokev_counter;
	uint32_t nokev_counter_freq;
	std::chrono::time_point<std::chrono::system_clock> last_nokev;
	std::chrono::time_point<std::chrono::system_clock> last_okev;
        std::chrono::time_point<std::chrono::steady_clock> last_ev;
        std::chrono::milliseconds periodic_ev;
	bool running;
	bool paused;
	bool stopped;
        std::vector<std::string> contexts;
        std::vector<std::string> contexts_upper;
	unsigned int ttl;
        std::shared_ptr<ReadersWritersLock> siglock;
}
HdbSignal;

class HdbDevice;
class SubscribeThread;


//=========================================================
/**
 *	Shared data between DS and thread.
 */
//=========================================================
//class SharedData: public omni_mutex
class SharedData: public Tango::TangoMonitor, public Tango::LogAdapter
{
private:
	/**
	 *	HdbDevice object
	 */
	HdbDevice	*hdb_dev;

	bool	stop_it;
        bool initialized;
        omni_mutex init_mutex;
        omni_condition init_condition;

        std::map<std::string, std::weak_ptr<HdbSignal>> signals_names;
        std::mutex cache_mutex;
public:
	int		action;
	//omni_condition condition;
        std::vector<std::shared_ptr<HdbSignal>>	signals;
	ReadersWritersLock      veclock;


	/**
	 * Constructor
	 */
	//SharedData(HdbDevice *dev):condition(this){ hdb_dev=dev; action=NOTHING; stop_it=false; initialized=false;};
	SharedData(HdbDevice *dev);
	~SharedData();
	/**
	 * Add a new signal.
	 */
	void add(const std::string &signame, const std::vector<std::string> & contexts, int ttl);
	void add(const std::string &signame, const std::vector<std::string> & contexts, int to_do, bool start, int ttl = DEFAULT_TTL);
	/**
	 * Remove a signal in the list.
	 */
	void remove(const std::string &signame, bool stop);
	/**
	 * Update contexts for a signal.
	 */
	void update(const std::string &signame, const std::vector<std::string> & contexts);
	/**
	 * Update ttl for a signal.
	 */
	void updatettl(const std::string &signame, unsigned int ttl);
	/**
	 * Start saving on DB a signal.
	 */
	void start(const std::string &signame);
	/**
	 * Pause saving on DB a signal.
	 */
	void pause(const std::string &signame);
	/**
	 * Stop saving on DB a signal.
	 */
	void stop(const std::string &signame);
	/**
	 * Start saving on DB all signals.
	 */
	void start_all();
	/**
	 * Pause saving on DB all signals.
	 */
	void pause_all();
	/**
	 * Stop saving on DB all signals.
	 */
	void stop_all();
	/**
	 * Is a signal saved on DB?
	 */
	auto is_running(const std::string &signame) -> bool;
	/**
	 * Is a signal saved on DB?
	 */
	auto is_paused(const std::string &signame) -> bool;
	/**
	 * Is a signal not subscribed?
	 */
	auto is_stopped(const std::string &signame) -> bool;
	/**
	 * Is a signal to be archived with current context?
	 */
	auto is_current_context(const std::string &signame, std::string context) -> bool;
	/**
	 * Is a signal first event arrived?
	 */
	auto is_first(const std::string &signame) -> bool;
	/**
	 * Set a signal first event arrived
	 */
	void set_first(const std::string &signame);
	/**
	 * Is a signal first consecutive error event arrived?
	 */
	auto is_first_err(const std::string &signame) -> bool;
	/**
	 * Set a signal first consecutive error event arrived
	 */
	void set_first_err(const std::string &signame);
	/**
	 *	get signal by name.
	 */
	auto get_signal(const std::string &name) -> std::shared_ptr<HdbSignal>;
	/**
	 * Subscribe achive event for each signal
	 */
	void subscribe_events();
	/**
	 * Unsubscribe achive event for each signal
	 */
	void unsubscribe_events();
	/**
	 *	return number of signals to be subscribed
	 */
	auto nb_sig_to_subscribe() -> int;
	/**
	 *	build a list of signal to set HDB device property
	 */
	void put_signal_property();
	/**
	 *	Return the list of signals
	 */
	void get_sig_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals on error
	 */
	void get_sig_on_error_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals not on error
	 */
	void get_sig_not_on_error_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals started
	 */
	void get_sig_started_list(std::vector<std::string> &);
	/**
	 *	Return the list of signals not started
	 */
	void get_sig_not_started_list(std::vector<std::string> &);
	/**
	 *	Return the list of errors
	 */
	auto get_error_list(std::vector<std::string> &) -> bool;
	/**
	 *	Return the list of event received
	 */
	void get_ev_counter_list(std::vector<uint32_t> &);
	/**
	 *	Return the number of signals on error
	 */
	auto  get_sig_on_error_num() -> int;
	/**
	 *	Return the number of signals not on error
	 */
	auto  get_sig_not_on_error_num() -> int;
	/**
	 *	Return the number of signals started
	 */
	auto  get_sig_started_num() -> int;
	/**
	 *	Return the number of signals not started
	 */
	auto  get_sig_not_started_num() -> int;
	/**
	 *	Return the complete, started and stopped lists of signals
	 */
	auto  get_lists(std::vector<std::string> &s_list, std::vector<std::string> &s_start_list, std::vector<std::string> &s_pause_list, std::vector<std::string> &s_stop_list, std::vector<std::string> &s_context_list, Tango::DevULong *ttl_list) -> bool;
	/**
	 *	Increment the ok counter of event rx
	 */
	void  set_ok_event(const std::string &signame);
	/**
	 *	Get the ok counter of event rx
	 */
	auto get_ok_event(const std::string &signame) -> uint32_t;
	/**
	 *	Get the ok counter of event rx for freq stats
	 */
	auto get_ok_event_freq(const std::string &signame) -> uint32_t;
	/**
	 *	Get last okev timestamp
	 */
	auto get_last_okev(const std::string &signame) -> std::chrono::time_point<std::chrono::system_clock>;
	/**
	 *	Increment the error counter of event rx
	 */
	void  set_nok_event(const std::string &signame);
	/**
	 *	Get the error counter of event rx
	 */
	auto get_nok_event(const std::string &signame) -> uint32_t;
	/**
	 *	Get the error counter of event rx for freq stats
	 */
	auto get_nok_event_freq(const std::string &signame) -> uint32_t;
	/**
	 *	Get last nokev timestamp
	 */
	auto get_last_nokev(const std::string &signame) -> std::chrono::time_point<std::chrono::system_clock>;
	/**
	 *	Set state and status of timeout on periodic event
	 */
	void  set_nok_periodic_event(const std::string& signame);
	/**
	 *	Return the status of specified signal
	 */
	auto get_sig_status(const std::string &signame) -> std::string;
	/**
	 *	Return the state of specified signal
	 */
	auto get_sig_state(const std::string &signame) -> Tango::DevState;
	/**
	 *	Return the contexts of specified signal
	 */
	auto get_sig_context(const std::string &signame) -> std::string;
	/**
	 *	Return the ttl of specified signal
	 */
	auto get_sig_ttl(const std::string &signame) -> Tango::DevULong;
	/**
	 *	Set Archive periodic event period
	 */
	void  set_conf_periodic_event(const std::string &signame, const std::string &period);
	/**
	 *	Check Archive periodic event period
	 */
	auto check_periodic_event_timeout(std::chrono::milliseconds delay_tolerance) -> std::chrono::milliseconds;
	/**
	 *	Reset statistic counters
	 */
	void reset_statistics();
	/**
	 *	Reset freq statistic counters
	 */
	void reset_freq_statistics();
	/**
	 *	Return ALARM if at list one signal is not subscribed.
	 */
	auto state() -> Tango::DevState;
	
	auto is_initialized() -> bool;
	auto get_if_stop() -> bool;
	void stop_thread();
	void wait_initialized();
};


//=========================================================
/**
 *	Create a thread retry to subscribe event.
 */
//=========================================================
class SubscribeThread: public omni_thread, public Tango::LogAdapter
{
private:
	/**
	 *	Shared data
	 */
	std::shared_ptr<SharedData> shared;
        const std::chrono::milliseconds period;
	/**
	 *	HdbDevice object
	 */
	HdbDevice	*hdb_dev;


public:
	SubscribeThread(HdbDevice *dev, std::chrono::seconds period);
	void updateProperty();
	/**
	 *	Execute the thread loop.
	 *	This thread is awaken when a command has been received 
	 *	and falled asleep when no command has been received from a long time.
	 */
	void *run_undetached(void *);
	void start() {start_undetached();}
};


}	// namespace_ns

#endif	// _SUBSCRIBE_THREAD_H
