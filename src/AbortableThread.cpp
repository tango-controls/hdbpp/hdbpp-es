//+=============================================================================
//
// file :         AbortableThread.cpp
//
// description :  A thread that run code in a loop till it is aborted
// project :      TANGO Device Server
//
// $Author: graziano $
//
// $Revision: 1.6 $
//
// $Log: CheckPeriodicThread.cpp,v $
//
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================


#include <AbortableThread.h>
#include <cmath>
#include "Consts.h"

namespace HdbEventSubscriber_ns
{

    //=============================================================================
    //=============================================================================
    AbortableThread::AbortableThread(Tango::DeviceImpl *dev, std::chrono::duration<double> p): Tango::LogAdapter(dev)
                                                              , abort_flag(false)
                                                              , period(p)
                                                              , abort_condition(&abort_mutex)
    {
    }

    //=============================================================================
    //=============================================================================
    AbortableThread::~AbortableThread() = default;
    /*
    {
       // join(nullptr);
    }
    */

    //=============================================================================
    //=============================================================================
    auto AbortableThread::run_undetached(void* /*unused*/) -> void *
    {
        init_abort_loop();

        bool aborted = abort_flag.load();

        while(!aborted)
        {

            run_thread_loop();

            // Temporisation
            aborted = timed_wait() != 0;
        }

        finalize_abort_loop();
        return nullptr;
    }

    //=============================================================================
    //=============================================================================
    auto AbortableThread::timed_wait() -> int
    {
        if(!abort_flag.load())
        {
            auto time = std::chrono::duration<double>::zero();

            if(period > std::chrono::duration<double>::zero())
            {
                time = get_period();
            }
            else
            {
                time = get_abort_loop_period();
            }

            // if timeout < 0 do not wait.
            if(time > std::chrono::duration<double>::zero())
            {
                unsigned long abs_sec = 0;
                unsigned long abs_nsec = 0;

                const auto rel_sec = std::chrono::duration_cast<std::chrono::seconds>(time);
                const auto rel_nsec = std::chrono::duration_cast<std::chrono::nanoseconds>(time - rel_sec);

                omni_thread::get_time(&abs_sec, &abs_nsec, rel_sec.count(), rel_nsec.count());
                return abort_condition.timedwait(abs_sec, abs_nsec);
            }
            // if not waiting return the abort_flag in case it was aborted
            return 0;
        }
        return 1;
    }

    //=============================================================================
    //=============================================================================
    void AbortableThread::abort()
    {
        abort_flag.store(true);

        abort_condition.signal();

        //in case extra steps are needed on abort
        do_abort();
    }
}//	namespace
