# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.2.1] - 2024-12-03

### Fixed

* Segfault when removing an attribute, cause an exception was not caught.
* Use only on callback and do not delete it. There was a bug where leading to this callback being nulled.
* TTL setup at startup was not done.
* Memory usage. Cache name checking with or without a fqtrl to reduce cpu usage.
* Build against tango 10

## [2.2.0] - 2023-10-18

### Added

* MacOs support
* Support for cppTango 9.4 and further

### Changed

* Handle conf event slightly better to avoid needless reconnections.
* Fixed a crash upon reconnection. With a callback being deleted twice
* Removed unused ifdef code for clarity.
* Fix an issue with a call to getaddrinfo with localhost.

### Removed
* Got rid of the information on the type of events (ZMQ, notifd) from the attribute_status command
  WARNING, if you were parsing the result of this command for whatever reason, your code might break
* Got rid of some exception being output to files.

## [2.1.0] - 2022-10-04

### Added

* Support batch insertion
* Build flag to choose which backend to build against.
* Update build mechanism to fetch libhdbpp implementation.
* DB_ADD command support to register an attribute in the db from the AttributeAdd command.
* New CMake build system that can download libhdbpp when requested
* Moved HDBCmdData source from libhdbpp project
* Clang integration
* AttributeListFile device property to read attribute list from file

### Changed

* Use chrono instead of clock_gettime
* In AttributeAdd command, only the attribute name is needed, if the other parameters
  are missing, a request to the database will be done to get them.
* Do not depend on HdbClient.h, but only AbstracDB.h
* Small refactoring and code modernization to get rid of some clang warnings.
* Updated README for new build system
* Observe new namespace in libhdbpp
* Changed libhdbpp includes to new path (hdb++) and new split header files
* Project now links directly to given libhdbpp soname major version
* Made compatible with new libhdbpp (namespace, function and path changes)

## [1.0.2] - 2019-09-23

### Added

* Subscribe to change event as fallback.

## [1.0.1] - 2018-02-28

### Fixed

* Segmentation fault when error updating ttl.

## [1.0.0] - 2017-09-28

### Added

* CHANGELOG.md file.
* Debian Package build files under debian/

### Changed

* Makefile: Added install rules, clean up paths etc
* Makefile: Cleaned up the linkage (removed unneeded libraries, libzmq, libCOS)
* Cleaned up some unused variable warnings
* libhdb++ include paths to match new install location.
* Updated README.md.

### Removed

* libhdbpp submodule. This now has to be installed manually.
